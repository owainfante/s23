let trainer = {
    name : "Owa",
    Age : 25,
    friends : {
        Kanto : ["Brock","Misty"],
        Kalos : ["Serena", "Clemont"]
    },
    pokemon : ["Pikachu","Arcanine","Sceptile","Oshawott"],
    talk : function(){
        console.log("Pikachu! I choose you!")
    }
}

console.log(trainer)
console.log("Result of Dot Notation:")
console.log(trainer.name)
console.log("Result of Square Bracket Notation:")
console.log(trainer["pokemon"])
console.log("Result of Talk Method")
trainer.talk()

function Pokemon(name,level,health,attack){
    this.name = name
    this.level = level
    this.health = health
    this.attack = attack

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name)
            target.health -= this.attack
            console.log(target.name + "'s health is now reduced to " + target.health)
        if(target.health <= 0) {
            this.faint(target)
        }
    }
    this.faint = function(target){
        console.log(target.name + " has fainted");
    }
}

console.log("My Pokemon")
let pikachu = new Pokemon("Pikachu", 25, 59, 39)
let oshawott = new Pokemon("Oshawott", 15, 55, 55)

console.log(pikachu)
console.log(oshawott)

console.log("Enemy Pokemon")
let opponent = new Pokemon("Onix", 32, 85, 45)
console.log(opponent)

console.log("Battle Text")
opponent.tackle(oshawott)
console.log(oshawott)
oshawott.tackle(opponent)
oshawott.tackle(opponent)
console.log(opponent)
